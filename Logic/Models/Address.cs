﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PeopleSearch.Logic.Models
{
    [Table("Addresses")]
    public class Address
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(60)]
        public string Line1 { get; set; }
        [StringLength(60)]
        public string Line2 { get; set; }
        [StringLength(60)]
        public string Line3 { get; set; }
        [Required]
        [StringLength(50)]
        public string City { get; set; }
        [Required]
        [StringLength(50)]
        public string State { get; set; }
        [Required]
        [StringLength(10)]
        public string PostalCode { get; set; }
    }
}
