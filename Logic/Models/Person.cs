﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PeopleSearch.Logic.Models
{
    [Table("People")]
    public class Person
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(25)]
        public string FirstName { get; set; }
        [Required]
        [StringLength(25)]
        public string LastName { get; set; }
        [Required]
        [Column(TypeName = "datetime2")]
        public DateTime DateOfBirth { get; set; }
        [StringLength(256)]
        public string Interests { get; set; }
        [StringLength(256)]
        public string ProfilePicUrl { get; set; }

        public virtual Address Address { get; set; }
    }
}
