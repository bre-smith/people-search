﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace PeopleSearch.Logic.Models
{
    public class PeopleSearchContext : DbContext
    {
        public PeopleSearchContext(DbContextOptions<PeopleSearchContext> options)
            : base(options)
        { }

        public PeopleSearchContext()
        {

        }

        public virtual DbSet<Person> People { get; set; }
        public virtual DbSet<Address> Addresses { get; set; }
    }
}
