﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using PeopleSearch.Logic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PeopleSearch.Logic.Services
{
    public class PeopleService
    {
        private readonly ILogger<PeopleService> logger;
        private readonly PeopleSearchContext db;
        private readonly SearchService searchService;

        public PeopleService(ILogger<PeopleService> logger, PeopleSearchContext db, SearchService searchService)
        {
            this.logger = logger;
            this.db = db;
            this.searchService = searchService;
        }

        public List<Person> GetAllPeople()
        {
            return db.People.Include("Address").ToList();
        }

        public Person GetPerson(int id)
        {
            return db.People.Include("Address").FirstOrDefault(p => p.Id == id);
        }

        public List<Person> SearchPerson(string term)
        {
            var people = GetAllPeople();

            //Indexing the entire DB in elasticsearch each request to simulate search slowness as per requirements.
            searchService.Index(people);

            var searchResult = searchService.SearchPerson(term);

            return db.People.Include("Address").Where(p => searchResult.Contains(p.Id)).ToList();
        }

        public Person CreatePerson(Person person)
        {
            var result = db.People.Add(person);
            db.SaveChanges();
            searchService.Index(result.Entity);
            return result.Entity;
        }

        public Person UpdatePerson(Person person)
        {
            var result = db.People.Update(person);
            db.SaveChanges();
            searchService.Update(result.Entity);
            return result.Entity;
        }

        public Person DeletePerson(int id)
        {
            var person = db.People.FirstOrDefault(p => p.Id == id);
            if (person != null)
            {
                var result = db.People.Remove(person);
                db.SaveChanges();
                searchService.Delete(result.Entity);
                return result.Entity;
            }

            return null;
        }
    }
}
