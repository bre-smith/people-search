﻿using Nest;
using PeopleSearch.Logic.Models;
using System.Collections.Generic;
using System.Linq;

namespace PeopleSearch.Logic.Services
{
    public class SearchService
    {
        private readonly IElasticClient elasticClient;
        public SearchService(IElasticClient elasticClient)
        {
            this.elasticClient = elasticClient;
        }

        public List<int> SearchPerson(string term)
        {
            var searchResponse = elasticClient.Search<Person>(s => s.Query(q => q.QueryString(c => c
                    .Fields(f => f.Field(a => a.FirstName).Field(b => b.LastName))
                    .Query("*" + term + "*").AllowLeadingWildcard())).Take(5000));

            if (searchResponse.IsValid)
            {
                return searchResponse.Documents.Select(r => r.Id).ToList(); ;
            }

            return new List<int>();
        }

        public void Delete(Person person)
        {
            elasticClient.Delete<Person>(person);
        }

        public void Index(Person person)
        {
            elasticClient.IndexDocument(person);
        }

        public void Index(List<Person> people)
        {
            people.ForEach(person =>
            {
                Index(person);
            });
        }

        public void Update(Person person)
        {
            elasticClient.Update<Person>(person, u => u.Doc(person));
        }
    }
}
