using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Nest;
using PeopleSearch.Logic.Models;
using PeopleSearch.Logic.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Logic.Tests
{
    [TestClass]
    public class PeopleServiceTests
    {
        [TestMethod]
        public void GetAllPeople_Should_Return_All_People()
        {
            //Arrange
            var mockDbContext = GetMockDbContext();
            var mockLogger = new Mock<ILogger<PeopleService>>();
            var mockElasticClient = new Mock<IElasticClient>();
            var mockSearchService = new Mock<SearchService>(mockElasticClient.Object);

            //Act - get all people
            var peopleService = new PeopleService(mockLogger.Object, mockDbContext.Object, mockSearchService.Object);
            var people = peopleService.GetAllPeople();

            //Assert
            Assert.AreEqual(2, people.Count());
            Assert.AreEqual("Test1", people.First().FirstName);
        }


        private Mock<PeopleSearchContext> GetMockDbContext()
        {
            // Arrange - We're mocking our dbSet & dbContext
            // in-memory data
            IQueryable<Person> people = new List<Person>
            {
                new Person
                {
                    FirstName = "Test1",
                    LastName = "User1",
                    DateOfBirth = DateTime.Now,
                    Interests = "Test Interest",
                    ProfilePicUrl = "http://google.com",
                    Address = new Address
                    {
                        Line1 = "123 Street",
                        City = "Test City",
                        State = "Test State",
                        PostalCode = "12345"
                    }
                },
                new Person
                {
                    FirstName = "Test2",
                    LastName = "User2",
                    DateOfBirth = DateTime.Now,
                    Interests = "Test Interest",
                    ProfilePicUrl = "http://google.com",
                    Address = new Address
                    {
                        Line1 = "123 Street",
                        City = "Test City",
                        State = "Test State",
                        PostalCode = "12345"
                    }
                }

            }.AsQueryable();

            // To query our database we need to implement IQueryable 
            var mockSet = new Mock<DbSet<Person>>();
            mockSet.As<IQueryable<Person>>().Setup(m => m.Provider).Returns(people.Provider);
            mockSet.As<IQueryable<Person>>().Setup(m => m.Expression).Returns(people.Expression);
            mockSet.As<IQueryable<Person>>().Setup(m => m.ElementType).Returns(people.ElementType);
            mockSet.As<IQueryable<Person>>().Setup(m => m.GetEnumerator()).Returns(people.GetEnumerator());

            var mockContext = new Mock<PeopleSearchContext>();
            mockContext.Setup(c => c.People).Returns(mockSet.Object);

            return mockContext;
        }
    }
}
