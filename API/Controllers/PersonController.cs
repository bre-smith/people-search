﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PeopleSearch.Logic.Models;
using PeopleSearch.Logic.Services;
using System;

namespace PeopleSearch.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonController : ControllerBase
    {
        private readonly PeopleService peopleService;
        private readonly ILogger<PersonController> logger;
        public PersonController(PeopleService peopleService, ILogger<PersonController> logger)
        {
            this.peopleService = peopleService;
            this.logger = logger;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok(peopleService.GetAllPeople());
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                if (id == 0)
                {
                    return BadRequest("id must be greater than 0");
                }
                return Ok(peopleService.GetPerson(id));
            }
            catch (Exception ex)
            {
                logger.LogError(ex, $"Error getting person with id: {id}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpGet]
        [Route("Search")]
        public IActionResult Search([FromQuery(Name = "term")] string term)
        {
            if (string.IsNullOrEmpty(term))
            {
                return BadRequest("Search term may not be null or empty");
            }
            return Ok(peopleService.SearchPerson(term));
        }

        [HttpPost]
        public IActionResult Post([FromBody] Person person)
        {
            if (person == null)
            {
                return BadRequest("person may not be null");
            }
            if (person.DateOfBirth > DateTime.Now)
            {
                return BadRequest("DateOfBirth may not be in the future");
            }
            if (person.DateOfBirth < DateTime.Now.AddYears(-150))
            {
                return BadRequest("DateOfBirth must be more recent");
            }
            return Ok(peopleService.CreatePerson(person));
        }

        [HttpPut("{id}")]
        public IActionResult Put([FromBody] Person person)
        {
            if (person == null)
            {
                return BadRequest("person may not be null");
            }
            if (person.DateOfBirth > DateTime.Now)
            {
                return BadRequest("DateOfBirth may not be in the future");
            }
            if (person.DateOfBirth < DateTime.Now.AddYears(-150))
            {
                return BadRequest("DateOfBirth must be more recent");
            }
            return Ok(peopleService.UpdatePerson(person));
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        { 
            if (id == 0)
            {
                return BadRequest("id must be greater than 0");
            }
            return Ok(peopleService.DeletePerson(id));
        }
    }
}
