import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PersonAddComponent } from './person-add/person-add.component';
import { PersonDetailComponent } from './person-detail/person-detail.component';
import { PersonEditComponent } from './person-edit/person-edit.component';
import { PersonSearchComponent } from './person-search/person-search.component';

const routes: Routes = [
  {
    path: 'people/search',
    component: PersonSearchComponent,
    data: { title: 'List of People' }
  },
  {
    path: 'people/add',
    component: PersonAddComponent,
    data: { title: 'Add Product' }
  },
  {
    path: 'people/:id',
    component: PersonDetailComponent,
    data: { title: 'Person Details' }
  },

  {
    path: 'people/edit/:id',
    component: PersonEditComponent,
    data: { title: 'Edit Product' }
  },
  {
    path: '',
    redirectTo: 'people/search',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
