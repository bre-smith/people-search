import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormBuilder, FormGroup, Validators, NgForm } from '@angular/forms';
import { Person } from '../person.model';
import { PersonService } from '../person.service';

@Component({
  selector: 'app-person-add',
  templateUrl: './person-add.component.html',
  styleUrls: ['./person-add.component.css']
})
export class PersonAddComponent implements OnInit {

  personForm: FormGroup;
  person: Person;
  isLoadingResults = false;

  constructor(private router: Router, private personService: PersonService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.personForm = this.formBuilder.group({
      firstName: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(25)]),
      lastName: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(25)]),
      dateOfBirth: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(25)]),
      interests: new FormControl('', [Validators.maxLength(256)]),
      profilePicUrl: new FormControl('', [Validators.maxLength(256)]),
      address: this.formBuilder.group({
        line1: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(60)]),
        line2: new FormControl(''),
        line3: new FormControl(''),
        city: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(50)]),
        state: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(50)]),
        postalCode: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(10)]),
      })
    });
  }

  onFormSubmit(form: NgForm) {
    this.isLoadingResults = true;
    this.personService.addPerson(form)
      .subscribe(res => {
        let id = res['id'];
        this.isLoadingResults = false;
        this.router.navigate(['/people', id]);
      }, (err) => {
        console.log(err);
        this.isLoadingResults = false;
      });
  }
}
