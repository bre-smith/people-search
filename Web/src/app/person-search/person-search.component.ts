import { Component, OnInit } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { PersonService } from '../person.service';
import { Person } from '../person.model';

@Component({
  selector: 'app-person-search',
  templateUrl: './person-search.component.html',
  styleUrls: ['./person-search.component.css']
})
export class PersonSearchComponent implements OnInit {

  displayedColumns: string[] = ['firstName', 'lastName', 'city', 'state', 'actions'];
  data: Person[] = [];
  searchForm: FormGroup;
  isLoadingResults = false;
  maxResults: number = 25;
  resultCount: number;

  constructor(private personService: PersonService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.searchForm = this.formBuilder.group({
      searchTerm: new FormControl('')
    });
  }

  onFormSubmit(term: string) {
    this.isLoadingResults = true;
    this.personService.searchPerson(term)
      .subscribe(res => {
        this.resultCount = res.length;
        this.data = res.slice(0,25);
        this.isLoadingResults = false;
      }, (err) => {
        console.log(err);
        this.isLoadingResults = false;
      }
      );
  }

}
