import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { PersonService } from '../person.service';
import { Person } from '../person.model';

@Component({
  selector: 'app-person-edit',
  templateUrl: './person-edit.component.html',
  styleUrls: ['./person-edit.component.css']
})
export class PersonEditComponent implements OnInit {

  personForm: FormGroup;
  person: Person;
  id: number;
  isLoadingResults = false;

  constructor(private router: Router, private route: ActivatedRoute, private personService: PersonService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.personForm = this.formBuilder.group({
      id: new FormControl(''),
      firstName: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(25)]),
      lastName: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(25)]),
      dateOfBirth: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(25)]),
      interests: new FormControl('', [Validators.maxLength(256)]),
      profilePicUrl: new FormControl('', [Validators.maxLength(256)]),
      address: this.formBuilder.group({
        id: new FormControl(''),
        line1: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(60)]),
        line2: new FormControl(''),
        line3: new FormControl(''),
        city: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(50)]),
        state: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(50)]),
        postalCode: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(10)]),
      })
    });
    this.getPerson(this.route.snapshot.params['id']);
  }

  getPerson(id) {
    this.personService.getPerson(id).subscribe(data => {
      this.id = data.id;
      this.personForm.setValue(data);
    });
  }

  onFormSubmit(form: NgForm) {
    this.isLoadingResults = true;
    this.personService.updatePerson(this.id, form)
      .subscribe(res => {
        let id = res['id'];
        this.isLoadingResults = false;
        this.router.navigate(['/people', id]);
      }, (err) => {
        console.log(err);
        this.isLoadingResults = false;
      }
    );
  }

  personDetails() {
    this.router.navigate(['/people', this.id]);
  }

}
