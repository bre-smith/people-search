import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Person } from '../person.model';
import { PersonService } from '../person.service';

@Component({
  selector: 'app-person-detail',
  templateUrl: './person-detail.component.html',
  styleUrls: ['./person-detail.component.css']
})
export class PersonDetailComponent implements OnInit {

  person: Person;
  isLoadingResults = false;

  constructor(private route: ActivatedRoute, private personService: PersonService, private router: Router) { }

  ngOnInit() {
    this.getPersonDetails(this.route.snapshot.params['id']);
  }

  getPersonDetails(id) {
    this.personService.getPerson(id)
      .subscribe(data => {
        this.person = data;
        console.log(this.person);
        this.isLoadingResults = false;
      });
  }

  deletePerson(id) {
    this.isLoadingResults = true;
    this.personService.deletePerson(id)
      .subscribe(res => {
        this.isLoadingResults = false;
        this.router.navigate(['/people/search']);
      }, (err) => {
        console.log(err);
        this.isLoadingResults = false;
      }
      );
  }

}
