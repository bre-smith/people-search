import { Address } from './address.model';

export class Person {
  id: number;
  firstName: string;
  lastName: string;
  dateOfBirth: Date;
  interests: string;
  profilePicUrl: string;
  address: Address
}
