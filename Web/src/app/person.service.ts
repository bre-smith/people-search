import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';

import { Person } from './person.model';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
const apiUrl = "http://localhost:4201/api/person";

@Injectable({
  providedIn: 'root'
})
export class PersonService {

  constructor(private http: HttpClient) { }

  getPeople(): Observable<Person[]> {
    return this.http.get<Person[]>(apiUrl)
      .pipe(
        tap(people => console.log('Fetch Person')),
        catchError(this.handleError('getPeople', []))
      );
  }

  getPerson(id: number): Observable<Person> {
    const url = `${apiUrl}/${id}`;
    return this.http.get<Person>(url).pipe(
      tap(_ => console.log(`fetched person id=${id}`)),
      catchError(this.handleError<Person>(`getPerson id=${id}`))
    );
  }

  searchPerson(term: string): Observable<Person[]> {
    const params = new HttpParams().set('term', term);
    return this.http.get<Person[]>(apiUrl + '/search', { params })
      .pipe(
        tap(people => console.log('Search Person')),
        catchError(this.handleError('searchPerson', []))
      );
  }

  addPerson(person): Observable<Person> {
    return this.http.post<Person>(apiUrl, person, httpOptions).pipe(
      tap((person: Person) => console.log(`added person w/ id=${person.id}`)),
      catchError(this.handleError<Person>('addPerson'))
    );
  }

  updatePerson(id, person): Observable<any> {
    const url = `${apiUrl}/${id}`;
    return this.http.put(url, person, httpOptions).pipe(
      tap(_ => console.log(`updated person id=${id}`)),
      catchError(this.handleError<any>('updatePerson'))
    );
  }

  deletePerson(id): Observable<Person> {
    const url = `${apiUrl}/${id}`;

    return this.http.delete<Person>(url, httpOptions).pipe(
      tap(_ => console.log(`deleted person id=${id}`)),
      catchError(this.handleError<Person>('deletePerson'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
